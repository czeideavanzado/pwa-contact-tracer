import { createMuiTheme } from "@material-ui/core/styles"

const ThemeGlobal = createMuiTheme({
  /**
   * COLORS!
   */
  palette: {
    background: "#5E35B1",
    primary: {
      main: "#00BCD4",
      light: "#62efff",
      dark: "#008ba3",
      contrastText: "#fffff",
    },
    secondary: {
      main: "#81C784",
      light: "#b2fab4",
      dark: "#519657",
      contrastText: "#fffff",
    },
  },

  /**
   * Override default styles
   */
  overrides: {
    // Style sheet name ⚛️
    MuiButton: {
      // Name of the rule
      root: {
        borderRadius: 100,
        border: 0,
        height: 40,
        textTransform: "inherit",
        textDecoration: "none",
      },
      label: {
        // Some CSS
        color: "white",
        fontStyle: "normal",
        fontWeight: "600",
        fontSize: 14,
      },
    },
  },

  /**
   * Fonts
   */
  typography: {
    color: "white",
    fontFamily: [
      "Work Sans",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
})

export default ThemeGlobal
