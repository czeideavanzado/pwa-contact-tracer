import React from 'react';
import Layout from "../components/skeleton/Layout"

const CheckIn = () => {

  return (
    <Layout>
      <section>
        Check In Form
      </section>
    </Layout>
  )
};

export default CheckIn;