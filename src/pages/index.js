import React from "react"

import Button from "@material-ui/core/Button"
import { makeStyles } from "@material-ui/core/styles"
import Typography from "@material-ui/core/Typography"

import SEO from "../components/skeleton/seo"
import Layout from "../components/skeleton/Layout"
import { Link } from 'gatsby';
import "../scss/index.scss"

const useStyles = makeStyles(theme => {
  return {
    root: {
      textAlign: "center",
      marginTop: theme.spacing(15),
    },
    container: {
      textAlign: "center",
    },
    main: {
      height: 419,
      position: "absolute",
      top: "45%",
      transform: "translateY(-50%)",
      width: "100vw",
    },
    desc: {
      textAlign: "center",
      color: theme.palette.common.white,
      width: 238,
      margin: "20px auto",
      "& h1": {
        fontWeight: 600,
        textShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
        fontSize: 24,
        lineHeight: 1.2,
      },
      "& h2": {
        lineHeight: 2,
      },
    },
    buttons: {
      textDecoration: "none",
      display: "block",
      margin: "0 auto",
      width: 260,
      "& button": {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        width: 260,
      },
    },
    typography: {
      color: 'white'
    }
  }
})

const IndexPage = () => {
  const classes = useStyles()
  return (
    <Layout>
      <SEO title="Landing" />
      <div className={classes.main}>
        <img className="sampleImg" alt="" src="images/landing/logo.png"></img>
        <div className={classes.desc}>
          <Typography variant="body1" component="h1" className={classes.typography}>
            Welcome to the
          </Typography>
          <Typography variant="body1" component="h1" className={classes.typography}>
            PH COVID-19 Tracer
          </Typography>
          <Typography variant="body2" component="h2" className={classes.typography}>
            Short description here
          </Typography>
        </div>
        <div className={classes.root}>
          <Link className={classes.buttons} to="/">
            <Button variant="contained" color="primary">
              Check In
            </Button>
          </Link>
          <Link className={classes.buttons} to="/">
            <Button variant="contained" color="secondary">
              View Trace Map
            </Button>
          </Link>
        </div>
      </div>
    </Layout>
  )
}

export default IndexPage
