import React from 'react';

const MainContainer = ({ children }) => {
  return (
    <main className="mainContent">
      {children}
    </main>
  )
}

export default MainContainer