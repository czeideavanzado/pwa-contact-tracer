/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import Helmet from "react-helmet"
import { withPrefix } from "gatsby"
import MainContainer from "./MainContainer"
import Header from "./Header"
import Footer from "./Footer"
import { ThemeProvider } from "@material-ui/core/styles"
import ThemeGlobal from "../../utilities/themeGlobal"
import CssBaseline from "@material-ui/core/CssBaseline"
import "../../scss/index.scss"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <>
        <ThemeProvider theme={ThemeGlobal}>
          <CssBaseline />
          <Header siteTitle={data.site.siteMetadata.title} />
            <MainContainer>
              {children}
            </MainContainer>
          <Footer />
        </ThemeProvider>
      <Helmet>
        <script
          async
          defer
          src={withPrefix("js/compressed.js")}
          type="text/javascript"
        />
      </Helmet>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
